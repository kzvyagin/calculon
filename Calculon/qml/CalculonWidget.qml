import QtQuick 2.0

Rectangle
{
    id: calculonRectangle
    anchors.fill: parent
    color : "#28b0d9"
    focus: true
    property string initialString : "WELCOME TO CALCULON";
    Component.onCompleted:
    {
        fillDataToModel()

    }


    function isHaveFuncInExpression()
    {
        var re = /[\+\-\*\/]/;
        if( textOutput.text.search(re) !== -1)
        {
            return true;

        }
        return false;
    }

    function isHaveDotInCurrentNumber()
    {
        var re = /[\+\-\*\/]/;
        var mid=textOutput.text.search(re);
        if(    mid === -1
            && textOutput.text.indexOf(".") !== -1 )
        {

            return true;
        }else if( mid > 0 )
        {

            var valueB=textOutput.text.slice(mid);

            if( valueB.indexOf(".") !== -1 )
            {
                return true;
            }
        }
        return false;
    }

    function processEnter()
    {
        vc.putExpressionIntoQueue( textOutput.text, Number.fromLocaleString( buttonsModel.getCommandDuration()) );
        textOutput.text=""
    }

    Keys.onPressed:
    {
        if(textOutput.text === calculonRectangle.initialString)
            textOutput.text="";

        if( event.key===Qt.Key_0 ) textOutput.text+=0;
        if( event.key===Qt.Key_1 ) textOutput.text+=1;
        if( event.key===Qt.Key_2 ) textOutput.text+=2;
        if( event.key===Qt.Key_3 ) textOutput.text+=3;
        if( event.key===Qt.Key_4 ) textOutput.text+=4;
        if( event.key===Qt.Key_5 ) textOutput.text+=5;
        if( event.key===Qt.Key_6 ) textOutput.text+=6;
        if( event.key===Qt.Key_7 ) textOutput.text+=7;
        if( event.key===Qt.Key_8 ) textOutput.text+=8;
        if( event.key===Qt.Key_9 ) textOutput.text+=9;


        if(event.key === Qt.Key_Minus    && isHaveFuncInExpression()  ===false && textOutput.text.length > 0) textOutput.text+="-";
        if(event.key === Qt.Key_Plus     && isHaveFuncInExpression()  ===false && textOutput.text.length > 0) textOutput.text+="+";
        if(event.key === Qt.Key_Asterisk && isHaveFuncInExpression()  ===false && textOutput.text.length > 0) textOutput.text+="*";
        if(event.key === Qt.Key_Slash    && isHaveFuncInExpression()  ===false && textOutput.text.length > 0) textOutput.text+="/";

        if(event.key === Qt.Key_Period   && isHaveDotInCurrentNumber() ===false ) textOutput.text+=".";

        if( ( event.key === Qt.Key_Equal || event.key ===Qt.Key_Enter || event.key ===Qt.Key_Return )
                && isHaveFuncInExpression() ===true )
        {
            processEnter();
        }
        //console.log("event.key=",event.key);
        event.accepted = true;
    }


    Connections {
        target: vc

        onQueueResultStatusChanged:
        {
            //console.log('qresstatus',qresstatus)
            buttonsModel.setResultQueueStatus(qresstatus);
        }

        onQueueRequestStatusChanged:
        {
            //console.log('qreqstatus',qreqstatus)
            buttonsModel.setRequestQueueStatus(qreqstatus);
        }
        onInformationToConsoleReady:
        {
            consoleListModel.append(newElement);
            consoleListView.positionViewAtEnd()
        }
    }


    ListModel {
        id: buttonsModel
        function setRequestQueueStatus( queueSize )
        {
            setQueueStatus(queueSize , "requestQueueStatus");
        }

        function setResultQueueStatus( queueSize )
        {
            setQueueStatus(queueSize , "resultQueueStatus");
        }

        function setQueueStatus( queueSize , iType )
        {
            for(var i=0; i<buttonsModel.count ;i++)
            {
                //  console.log(i,buttonsModel.get(i).iType,(buttonsModel.get(i).iType==="requestQueueStatus" ))
                if(buttonsModel.get(i).iType===iType )
                {
                    buttonsModel.get(i).iValue=""+queueSize;
                    return;
                }
            }
        }

        function getCommandDuration()
        {
            for(var i=0; i<buttonsModel.count ;i++)
            {
                if(buttonsModel.get(i).iType==="commandDuration" )
                {
                    return Number.fromLocaleString(buttonsModel.get(i).iValue)
                }
            }
            return 0;

        }
    }





    ListModel {
        id: consoleListModel
    }

    function fillDataToModel()
    {

        buttonsModel.append( { iColor: "#ccccb3",    iValue:""   , iType : "requestQueueStatus" } )
        buttonsModel.append( { iColor: "#e0e0d1",    iValue:""   , iType : "resultQueueStatus"  } )
        buttonsModel.append( { iColor: "#dfff80",    iValue:"5"  , iType : "commandDuration"  } )

        buttonsModel.append( { iColor: "#cc99ff",    iValue:"AC" , iType : "command" } )

        buttonsModel.append( { iColor: "#cc99ff",    iValue:"7" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"8" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"9" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"/" , iType : "function" } )

        buttonsModel.append( { iColor: "#cc99ff",    iValue:"4" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"5" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"6" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"*" , iType : "function" } )

        buttonsModel.append( { iColor: "#cc99ff",    iValue:"1" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"2" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"3" , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"-" , iType : "function" } )

        buttonsModel.append( { iColor: "#cc99ff",    iValue:"0"  , iType : "digit" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"."  , iType : "period" } )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"="  , iType : "command"} )
        buttonsModel.append( { iColor: "#cc99ff",    iValue:"+"  , iType : "function" } )

    }




    Component
    {
        id: buttondelegate
        Item {
            width: grid.cellWidth;
            height: grid.cellHeight

            Rectangle {
                id: im
                width: grid.cellWidth - 10;
                height: grid.cellHeight - 10
                color: iColor
                visible: true;
                border.color: "white"
                border.width: 0

                Text
                {
                    id: imText
                    anchors.fill: parent

                    text:  iValue
                    color: "black"
                    font.family: "Helvetica"
                    font.pointSize: 25
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment:  Text.AlignVCenter
                }





            }


        }
    }

    Column
    {
        spacing: 5

        Rectangle
        {
            id: textOutRectangle
            color: "#ccffcc"
            height: 40
            width: parent.parent.width
            Text
            {
                id: textOutput
                anchors.fill: parent
                anchors.margins: 3
                text: calculonRectangle.initialString
                color: "black"
                font.family: "Helvetica"
                font.pointSize: 20
                horizontalAlignment: Text.AlignRight

            }

        }



        Rectangle
        {
            color: "white"
            height: parent.parent.height -textOutRectangle.height-parent.spacing*2 -someConsolRectangle.height
            width: parent.parent.width


            GridView {
                id: grid
                property int rowElementsCount: 4
                property int colElementsCount: 5
                interactive: false // no flickable
                anchors.fill: parent
                anchors.topMargin: 5

                cellWidth: parent.width / rowElementsCount;
                cellHeight: parent.height /colElementsCount;

                model: buttonsModel
                delegate: buttondelegate

                //
                Item {
                    id: container
                    anchors.fill: parent
                }

                MouseArea {
                    id: coords
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    onPressed: {
                        var index=grid.indexAt(mouseX, mouseY)
                        var element=buttonsModel.get(index)
                        //console.log("element.iColor="+element.iColor+" iValue="+element.iValue+" iType="+element.iType);
                        //console.log("isHaveFuncInExpression()=",isHaveFuncInExpression());

                        if( element.iType === "commandDuration"   )
                        {
                             if(mouse.button === Qt.LeftButton)
                            {
                                element.iValue=""+(Number.fromLocaleString(element.iValue)-1);

                            }else
                            if(  mouse.button === Qt.RightButton )
                            {
                                element.iValue=""+(Number.fromLocaleString(element.iValue)+1);
                            }

                            if(   Number.fromLocaleString(element.iValue) < 0  )
                            {
                                element.iValue="0";
                            }

                            return;
                        }

                        if(textOutput.text === calculonRectangle.initialString)
                            textOutput.text="";

                        if(    ( element.iType === "command"  && element.iValue==="=") &&  isHaveFuncInExpression()===true )
                        {
                            processEnter();
                            return;
                        }

                        if( element.iType === "digit" )
                        {
                            textOutput.text+=element.iValue
                        }

                        //console.log("textOutput.text=",textOutput.text.length)
                        if( element.iType === "function" && isHaveFuncInExpression()===false && textOutput.text.length > 0)
                        {
                            textOutput.text+=element.iValue
                        }

                        if( element.iType === "command" && element.iValue==="AC" )
                        {
                            textOutput.text="";
                        }

                        if( element.iType === "period" && element.iValue==="."
                                && isHaveDotInCurrentNumber() ===false )
                        {
                            textOutput.text+=".";
                        }


                    }

                    onWheel:
                    {
                        var index=grid.indexAt(wheel.x, wheel.y)
                        var element=buttonsModel.get(index)
                        if( element.iType === "commandDuration"   )
                        {
                            if(wheel.angleDelta.y  > 0)
                            {
                                element.iValue=""+(Number.fromLocaleString(element.iValue)+1);
                            }else
                            {
                                element.iValue=""+(Number.fromLocaleString(element.iValue)-1);

                            }
                            if(   Number.fromLocaleString(element.iValue) < 0  )
                            {
                                element.iValue="0";
                            }
                        }

                    }




                }
            }
        }


        Rectangle
        {
            id: someConsolRectangle
            color: "#feffcc"
            height: 100
            width: parent.parent.width

            ListView {
                id: consoleListView
                anchors.fill: parent
                model: consoleListModel
                delegate: Rectangle
                {
                height: 16
                width: parent.parent.width
                color: iBackColor
                Text { height: 16 ; width: parent.width; text: iText; color: iColor   }
            }
        }


    }

}




}

