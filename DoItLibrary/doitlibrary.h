#ifndef DOITLIBRARY_H
#define DOITLIBRARY_H

#include "doitlibrary_global.h"

enum Work
{
    NONE=0,
    ADDING=1,
    SUBTRACTION=2,
    DIVISION=3,
    MULTIPLICATION=4
};

enum Error
{
    NO_ERROR=0,
    INVALID_TYPE_OF_WORK=1,
    INVALID_OPERATION=2,
    INFINITY_REZULT=3,
    NONE_RESULT=4,
    DIVISION_BY_ZERO=5
};
DOITLIBRARYSHARED_EXPORT double DoIt (int TypeWork, double OperandA, double OperandB, int &ErrorCode);

#endif // DOITLIBRARY_H
