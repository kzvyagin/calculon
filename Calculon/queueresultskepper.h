#ifndef QUEUERESULTSKEPPER_H
#define QUEUERESULTSKEPPER_H

#include <QObject>
#include <QDateTime>
#include "doitfacade.h"
#include <QList>
#include <QMutex>
#include "stctypes.h"
using namespace Stc;

class QueueResultsKepper : public QObject
{
    Q_OBJECT
public:
    static QueueResultsKepper& Instance();
private:
    QueueResultsKepper();
    QueueResultsKepper(const QueueResultsKepper& root);
    QueueResultsKepper& operator=(const QueueResultsKepper&);

signals:
    void sizeChanged(int);
public slots:
    void addResult( Result result);
    Result takeResult();
    int size();
private:
    QMutex mutex;
    QList<Result> QueueResults;
};

#endif // QUEUERESULTSKEPPER_H
