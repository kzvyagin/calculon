#include "viewcontroller.h"

#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QRegExp>
#include <QSettings>
#include <QStringList>
#include <QString>

ViewController::ViewController(QObject *parent) : QObject(parent),settingsTimer(0)
{

}

ViewController::~ViewController()
{
    flushGeometryToFile();
}

void ViewController::putExpressionIntoQueue(QString expression,int duration)
{
    // QString in function parametr just for example of RegExp usage, regular way to use putExpressionIntoQueueAsJson
    qDebug()<<Q_FUNC_INFO<<" expression="<< expression;

    QRegExp re("([+-]?\\d*\\.?\\d+)([/*\\-+]{1})([+-]?\\d*\\.?\\d+)");
    if( re.indexIn(expression)  != -1)
    {
        double valueA=re.cap(1).toDouble();
        QString operation=re.cap(2);
        double valueB=re.cap(3).toDouble();
        Stc::Request request;
        request.valueA=valueA;
        request.valueB=valueB;
        if(operation.contains("+"))
            request.commandType=Stc::Types::ADDING;
        if(operation.contains("-"))
            request.commandType=Stc::Types::SUBTRACTION;
        if(operation.contains("*"))
            request.commandType=Stc::Types::MULTIPLICATION;
        if(operation.contains("/"))
            request.commandType=Stc::Types::DIVISION;


        request.durationSeconds=duration;

        if(request.durationSeconds==0)
        {
            emit rizeErrorToConsole("TIME SPAN IS ZERO");
            return;
        }

        if(request.commandType==Stc::Types::NONE)
        {
            emit rizeErrorToConsole("INVALID COMMAND");
            return;
        }

        qDebug()<<Q_FUNC_INFO<<request.toString();
        emit requestReleased(request);
        return;
    }
    emit rizeErrorToConsole("INVALID EXPRESSION: "+expression.split(":").value(0));
    return;
}

QString ViewController::putExpressionIntoQueueAsJson(QJsonObject obj)
{
    QVariantMap map=obj.toVariantMap();
    Q_UNUSED(map);
    //TODO write implementation

    return QString();
}

QJsonObject ViewController::getGeometrySize()
{
    QString geometryString=QSettings().value("geometry","100,100,400,480").toString();
    QStringList list=geometryString.split(",");
    int x=list.value(0,"100").toInt();
    int y=list.value(1,"100").toInt();

    int width=list.value(2,"400").toInt();
    int height=list.value(3,"480").toInt();

    QVariantMap geometry;
    geometry.insert("x",x);
    geometry.insert("y",y);
    geometry.insert("width",width);
    geometry.insert("height",height);

    saveGeometry(QJsonObject().fromVariantMap( geometry ));
    return QJsonObject().fromVariantMap( geometry );
}

void ViewController::saveGeometry(QJsonObject size)
{
    if(geometryString.isEmpty())
        geometryString=QSettings().value("geometry","100,100,400,480").toString();
    QStringList list=geometryString.split(",");
    while( list.size() < 4 )
    {
        list<<"";
    }

    QVariantMap geometry=size.toVariantMap();
    if( geometry.contains("x")  )
    {
        list[0]=QString::number( geometry.value("x").toInt() );
    }
    if( geometry.contains("y")  )
    {
        list[1]=QString::number( geometry.value("y").toInt() );
    }
    if( geometry.contains("width")  )
    {
        list[2]=QString::number( geometry.value("width").toInt() );
    }
    if( geometry.contains("height")  )
    {
        list[3]=QString::number( geometry.value("height").toInt() );
    }

    geometryString=list.join(",");

    if(settingsTimer == 0 )
    {
        settingsTimer=startTimer(5000);
    }
}

void ViewController::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    killTimer(settingsTimer);
    settingsTimer=0;
    flushGeometryToFile();
}

void ViewController::flushGeometryToFile()
{
    QSettings s;
    s.setValue("geometry",geometryString);
}
