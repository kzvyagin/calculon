#ifndef DOITFACADE_H
#define DOITFACADE_H

#include "stctypes.h"

class DoItFacade
{
public:
    DoItFacade();
    static  double makeCalculation (Stc::Types::WorkType work, double operandA, double operandB, Stc::Types::Error &error);

};

#endif // DOITFACADE_H
