TEMPLATE = app

QT += qml quick concurrent
CONFIG += c++11

SOURCES += main.cpp \
    queuerequestskeeper.cpp \
    queueresultskepper.cpp \
    calculateworker.cpp \
    doitfacade.cpp \
    stctypes.cpp \
    queuerequestsworker.cpp \
    viewcontroller.cpp \
    workertwo.cpp

RESOURCES += qml.qrc

INCLUDEPATH += ../DoItLibray

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

LIBS +=   -lDoItLibrary
LIBS +=   -L../DoItLibrary  -L$$PWD/../DoItLibrary

INCLUDEPATH += $$PWD/../DoItLibrary
DEPENDPATH += $$PWD/../DoItLibrary

HEADERS += \
    queuerequestskeeper.h \
    queueresultskepper.h \
    calculateworker.h \
    doitfacade.h \
    stctypes.h \
    queuerequestsworker.h \
    viewcontroller.h \
    workertwo.h
