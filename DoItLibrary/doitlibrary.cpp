#include "doitlibrary.h"
#include <qalgorithms.h>

double DoIt(int TypeWork, double OperandA, double OperandB, int &ErrorCode)
{
    Work work=static_cast<Work>(TypeWork);

    if( work == 0 )
    {
        ErrorCode=static_cast<int>(Error::INVALID_TYPE_OF_WORK);
        return 0;
    }

    if( work==DIVISION &&  qFuzzyCompare(1+OperandB,1.+0.0)  )
    {
        ErrorCode=static_cast<int>(Error::DIVISION_BY_ZERO);
        return 0;
    }



    double result;

    switch (work)
    {
    case ADDING:
        result=OperandA+OperandB;
        break;
    case SUBTRACTION:
        result=OperandA-OperandB;
        break;
    case DIVISION:
        result=OperandA/OperandB;
        break;
    case MULTIPLICATION:
        result=OperandA*OperandB;
        break;
    default:
        ErrorCode=static_cast<int>(Error::INVALID_TYPE_OF_WORK);
        return 0;
        break;
    }

    if( qIsInf(result) )
    {
        ErrorCode=static_cast<int>(Error::INFINITY_REZULT);
        return result;
    }

    if( qIsNaN(result) )
    {
        ErrorCode=static_cast<int>(Error::NONE_RESULT);
        return result;
    }

    ErrorCode=static_cast<int>(Error::NO_ERROR);
    return result;
}
