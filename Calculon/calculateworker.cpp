#include "calculateworker.h"
#include "queuerequestskeeper.h"
#include "queueresultskepper.h"
#include "doitfacade.h"
#include <QtConcurrent>
#include <QFuture>
#include <QFutureWatcher>
#include <QDebug>
#include <QGuiApplication>
#include "queuerequestsworker.h"
#include "workertwo.h"
#include <QColor>
#include<QVariantMap>

//Запросы - зеленые, результаты - синие, ошибки - красные
QColor  CalculateWorker::ConsoleColors::BACK_ERROR = QColor("#ffe6e6");
QColor  CalculateWorker::ConsoleColors::BACK_RESULT =QColor("#e6f2ff");
QColor  CalculateWorker::ConsoleColors::BACK_REQUEST=QColor("#e6ffcc");

QColor  CalculateWorker::ConsoleColors::ERROR = QColor("red");
QColor  CalculateWorker::ConsoleColors::RESULT =QColor("blue");
QColor  CalculateWorker::ConsoleColors::REQUEST=QColor("green");

///
/// \brief The processRequestPredicate struct
/// as example , not used
struct processRequestPredicate
{
    Result operator ()( Request request )
    {
        Result result;
        result.result=DoItFacade::makeCalculation(request.commandType,request.valueA,request.valueB,result.errorCode);

        while( QDateTime::currentDateTime() <= result.ended.addSecs(request.durationSeconds))
        {
            QGuiApplication::processEvents();
        }

        return result;
    }
};


CalculateWorker::CalculateWorker(QObject *parent) : QObject(parent)
{
    connect(&QueueRequestsKeeper::Instance(),SIGNAL(sizeChanged(int)),this,SLOT(handleRequestsizeChanged(int)),Qt::QueuedConnection);
    connect(&QueueResultsKepper::Instance(),SIGNAL(sizeChanged(int)),this,SLOT(handleResultSizeChanged(int)),Qt::QueuedConnection);
    initThreads();
}


void CalculateWorker::initThreads()
{
    QThread* threadRequests = new QThread;
    QueueRequestsWorker* workerReq = new QueueRequestsWorker();
    workerReq->setObjectName("QueueRequestsWorker");
    workerReq->moveToThread(threadRequests);
    connect(workerReq, SIGNAL (error(QString)), this, SLOT (errorHandler(QString)));
    connect(threadRequests, SIGNAL (started()), workerReq, SLOT (process()));
    connect(workerReq, SIGNAL (finished()), threadRequests, SLOT (quit()));
    connect(workerReq, SIGNAL (finished()), workerReq, SLOT (deleteLater()));
    connect(threadRequests, SIGNAL (finished()), threadRequests, SLOT (deleteLater()));

    QThread* threadResults = new QThread;
    WorkerTwo* workerTwo = new WorkerTwo();
    workerTwo->setObjectName("workerTwo");
    workerTwo->moveToThread(threadResults);
    connect(workerTwo, SIGNAL (error(QString)), this, SLOT (errorHandler(QString)));
    connect(threadResults, SIGNAL (started()), workerTwo, SLOT (process()));
    connect(workerTwo, SIGNAL (finished()), threadResults, SLOT (quit()));
    connect(workerTwo, SIGNAL (finished()), workerTwo, SLOT (deleteLater()));
    connect(threadResults, SIGNAL (finished()), threadResults, SLOT (deleteLater()));


    connect(workerReq,SIGNAL(resultReady()),workerTwo,SLOT(handleResultReady()),Qt::QueuedConnection);
    connect(workerTwo,SIGNAL(resultReady(Stc::Result)),this,SLOT(handleResult(Stc::Result)),Qt::QueuedConnection);
    connect(workerTwo,SIGNAL(handleRequest()),workerReq,SLOT(processNewData()),Qt::QueuedConnection);

    connect(this,SIGNAL(requestReleased(Stc::Request)),workerTwo,SLOT(handleRequest(Stc::Request)),Qt::QueuedConnection);
    connect(workerTwo,SIGNAL(requestAddedToQueue(Stc::Request)),this,SLOT(logRequest(Stc::Request)));



    threadRequests->start();
    threadResults->start();

}

void CalculateWorker::errorHandler(QString error)
{
    qDebug()<<Q_FUNC_INFO<<sender()->objectName()<<error;
    sender()->deleteLater();
}


///
/// \brief work
/// as example, not used
void CalculateWorker::work()
{
    Request request=QueueRequestsKeeper::Instance().takeRequest();
    if( request.isValid() )
    {
        QFutureWatcher<Result> *watcher =new QFutureWatcher<Result>();
        connect(watcher, SIGNAL(finished()), SLOT(futureFinished()));
        QFuture< Result > future=QtConcurrent::run( processRequestPredicate(),request );
        watcher->setFuture(future);
    }
}

///
/// \brief futureFinished
/// as example, not used
void CalculateWorker::futureFinished()
{
    QFutureWatcher<Result> *watcher=dynamic_cast< QFutureWatcher<Result> *>( sender() );
    if( !watcher )
    {
        qDebug()<<Q_FUNC_INFO<<"Can't cast pointer to future watcher";
        return;
    }
    Result result=watcher->result();
    QueueResultsKepper::Instance().addResult(result);
    qDebug()<<Q_FUNC_INFO<<"Result done, emit event"<<result.result<<result.errorCode;
    emit resultComplite();
    watcher->deleteLater();
}

void CalculateWorker::handleResult(Result result)
{
    qDebug()<<Q_FUNC_INFO<<result.toString();

    QString res;

    if(result.errorCode==Types::NO_ERROR)
    {
        res=QString::number(result.result);
    }else
    {
        res="";
    }

    if(result.errorCode==Stc::Types::NO_ERROR)
        putInformationToViewConsole(result.toString(),ConsoleColors::RESULT,ConsoleColors::BACK_RESULT);
    else
        putInformationToViewConsole(result.toString(),ConsoleColors::ERROR,ConsoleColors::BACK_ERROR);

    emit resultToView(res);

}

void CalculateWorker::logRequest(Request request)
{
    putInformationToViewConsole(request.toString(),ConsoleColors::REQUEST,ConsoleColors::BACK_REQUEST);
}

void CalculateWorker::handleResultSizeChanged(int size)
{
    emit queueResultStatusChanged(QString::number(size));
}

void CalculateWorker::handleRequestsizeChanged(int size)
{
    emit queueRequestStatusChanged(QString::number(size));
}

void CalculateWorker::handleViewConrtollerError(QString errorText)
{
    putInformationToViewConsole(errorText,ConsoleColors::ERROR,ConsoleColors::BACK_ERROR);
}



void CalculateWorker::putInformationToViewConsole(QString text, QColor color, QColor backColor)
{
    QVariantMap map;
    map.insert(QString("iColor"),color);
    map["iText"]=text;
    map["iBackColor"]=backColor;

    emit informationToConsoleReady( QJsonObject().fromVariantMap(map));

}
