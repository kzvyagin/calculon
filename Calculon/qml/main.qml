import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
import "CommonWidgets.js" as Widgets

Window
{
    id:  rootWindow
    visible: true
    width: vc.getGeometrySize().width
    height:vc.getGeometrySize().height
    x:vc.getGeometrySize().x
    y:vc.getGeometrySize().y

    title: qsTr("Calculon")

    Rectangle
    {
        id: desktopScreen
        anchors.fill: parent

        Component.onCompleted:
        {
            createCalculonWigdet();

        }

        function createCalculonWigdet(){
            var object =  Widgets.CalculonWidget.createObject( desktopScreen , {    visible: true} )
            object.z=1
            desktopScreen.z=-1
            console.log("done");
        }

    }
    onWidthChanged:  { vc.saveGeometry({"x":x,"y":y,"width":width,"height":height }); }
    onHeightChanged: { vc.saveGeometry({"x":x,"y":y,"width":width,"height":height }); }
    onXChanged:      { vc.saveGeometry({"x":x,"y":y,"width":width,"height":height }); }
    onYChanged:      { vc.saveGeometry({"x":x,"y":y,"width":width,"height":height }); }
}
