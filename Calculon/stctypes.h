#ifndef STCTYPES_H
#define STCTYPES_H
#include <QDateTime>
#include <QtAlgorithms>
#include <QMetaType>
#include <QString>

namespace Stc
{

namespace Types
{

    enum WorkType
    {
        NONE=0,
        ADDING=1,
        SUBTRACTION=2,
        DIVISION=3,
        MULTIPLICATION=4
    };

    enum Error
    {
        NO_ERROR=0,
        INVALID_TYPE_OF_WORK=1,
        INVALID_OPERATION=2,
        INFINITY_REZULT=3,
        NONE_RESULT=4,
        DIVISION_BY_ZERO=5
    };
}

struct Request
{
    int uid;
    double    durationSeconds;
    Types::WorkType      commandType;
    double    valueA;
    double    valueB;
    QDateTime created;
    Request();
    QString toString() const;
    bool isValid();
};

struct Result
{
    int uid;
    Types::Error errorCode;
    double result;
    QDateTime ended;
    Result();
    QString errorToString();
    QString toString();
    bool isValid();
};

}

Q_DECLARE_METATYPE(Stc::Result)
Q_DECLARE_METATYPE(Stc::Request)


#endif // STCTYPES_H
