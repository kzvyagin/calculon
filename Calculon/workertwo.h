#ifndef WORKERTWO_H
#define WORKERTWO_H

#include <QObject>
#include "stctypes.h"
class WorkerTwo : public QObject
{
    Q_OBJECT
public:
    explicit WorkerTwo(QObject *parent = 0);

signals:
    void finished();
    void error(QString error);
    void resultReady( Stc::Result result );
    void handleRequest();
    void requestAddedToQueue( Stc::Request result );
public slots:
    void process();
    void handleResultReady();
    void handleRequest( Stc::Request request);

private:

};

#endif // WORKERTWO_H
