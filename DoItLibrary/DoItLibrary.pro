#-------------------------------------------------
#
# Project created by QtCreator 2017-07-17T21:51:07
#
#-------------------------------------------------

QT       += core
QT       -=gui

TARGET = DoItLibrary
TEMPLATE = lib

DEFINES += DOITLIBRARY_LIBRARY

SOURCES += doitlibrary.cpp

HEADERS += doitlibrary.h\
        doitlibrary_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
