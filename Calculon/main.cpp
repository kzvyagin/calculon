#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "doitlibrary.h"
#include <QDebug>
#include "queuerequestskeeper.h"
#include "queueresultskepper.h"
#include "calculateworker.h"
#include "stctypes.h"
#include <QTextCodec>
#include <QScreen>
#include <QtGui/QGuiApplication>
#include <QQmlContext>
#include <QQuickView>
#include "viewcontroller.h"



int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setOrganizationName("Zvyagin");
    app.setApplicationName("Calculon");
    qRegisterMetaType< Stc::Request >();
    qRegisterMetaType< Stc::Result >();

    QueueRequestsKeeper::Instance();
    QueueResultsKepper::Instance();
    CalculateWorker worker;
    QQmlApplicationEngine engine;

    ViewController *vc =new ViewController(&app);
    QObject::connect(vc,SIGNAL(requestReleased(Stc::Request)),&worker,SIGNAL(requestReleased(Stc::Request)));
    QObject::connect(&worker,SIGNAL(resultToView(QString)),vc,SIGNAL(resultReady(QString)));
    QObject::connect(&worker,SIGNAL(queueRequestStatusChanged(QString)),vc,SIGNAL(queueRequestStatusChanged(QString)));
    QObject::connect(&worker,SIGNAL(queueResultStatusChanged(QString)),vc,SIGNAL(queueResultStatusChanged(QString)));
    QObject::connect(&worker,SIGNAL(informationToConsoleReady(QJsonObject)),
                     vc,SIGNAL(informationToConsoleReady(QJsonObject)));
    QObject::connect(vc,SIGNAL(rizeErrorToConsole(QString)),
                     &worker,SLOT(handleViewConrtollerError(QString)));

    engine.rootContext()->setContextProperty( "vc",  vc);
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
