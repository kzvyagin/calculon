#ifndef QUEUEREQUESTSWORKER_H
#define QUEUEREQUESTSWORKER_H

#include <QObject>
#include <QString>
#include "stctypes.h"
using namespace Stc;

class QueueRequestsWorker : public QObject
{
    Q_OBJECT
public:
    explicit QueueRequestsWorker(QObject *parent = 0);
    ~QueueRequestsWorker();
signals:
    void finished();
    void error(QString error);
    void resultReady();
public slots:
    void process();

    void processNewData();

protected:
    void timerEvent(QTimerEvent *event);
private:
    void handleIt();
    void processRequest(  Request request);
    int  processTimerId;
    QHash <int,Result> buffer;

};

#endif // QUEUEREQUESTSWORKER_H
