#include "queueresultskepper.h"
#include <QMutexLocker>
QueueResultsKepper &QueueResultsKepper::Instance()
{
    static QueueResultsKepper instance;
    return instance;
}

QueueResultsKepper::QueueResultsKepper():QObject(0){}

void QueueResultsKepper::addResult(Result result)
{
    QMutexLocker locker(&mutex);
    QueueResults.push_back(result);
    emit sizeChanged(QueueResults.size());
}

Result QueueResultsKepper::takeResult()
{
    QMutexLocker lock(&mutex);
    if(QueueResults.isEmpty())
        return Result();

    auto var=QueueResults.takeFirst();
    emit sizeChanged(QueueResults.size());
    return var;
}

int QueueResultsKepper::size()
{
    QMutexLocker lock(&mutex);
    return QueueResults.size();
}

