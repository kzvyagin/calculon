#include "stctypes.h"
#include <QTextStream>
#include <QtGlobal>
namespace Stc
{

namespace Types
{

}

Request::Request():
    uid(1),
    durationSeconds(0),
    commandType(Types::NONE),
    valueA(0),
    valueB(0),
    created(QDateTime::currentDateTime())
{

}

QString Request::toString() const
{
    QString result;
    QTextStream str(&result);
    QString commandName;

    switch (commandType)
    {
    case Types::NONE:
        commandName="NONE";
        break;
    case Types::ADDING:
        commandName="ADDING";
        break;
    case Types::SUBTRACTION:
        commandName="SUBTRACTION";
        break;
    case Types::DIVISION:
        commandName="DIVISION";
        break;
    case Types::MULTIPLICATION:
        commandName="MULTIPLICATION";
        break;
    default:
        commandName="NOT SET";
        break;
    }
    str<<"REQUEST:"
       <<" uid:"<<uid
       <<" "<<commandName
       <<" valueA="<<valueA
       <<" valueB="<<valueB
       <<" duration="<<durationSeconds
       <<" created="<<created.toString("hh:mm:ss");
    return result;
}

bool Request::isValid()
{
    return commandType != Types::NONE && durationSeconds > 0;
}

Result::Result():
    uid(1),
    errorCode(Types::NO_ERROR),
    result(qQNaN()),
    ended(QDateTime::currentDateTime()){}

QString Result::errorToString()
{
    QString errorName;
    switch (errorCode)
    {
    case Types::NO_ERROR:
        errorName="NO_ERROR";
        break;
    case Types::INVALID_TYPE_OF_WORK:
        errorName="INVALID_TYPE_OF_WORK";
        break;
    case Types::INVALID_OPERATION:
        errorName="INVALID_OPERATION";
        break;
    case Types::INFINITY_REZULT:
        errorName="INFINITY_REZULT";
        break;
    case Types::NONE_RESULT:
        errorName="NONE_RESULT";
        break;
    case Types::DIVISION_BY_ZERO:
        errorName="DIVISION_BY_ZERO";
        break;
    default:
        errorName="NOT SET";
        break;
    }
    return errorName;
}

QString Result::toString()
{
    QString result;
    QTextStream str(&result);
    QString errorName=errorToString();
    str<<"RESULT:"
       <<" uid:"<<uid
       <<" "<<errorName
       <<" result="<<(qIsNaN(this->result)? "NaN":QString::number(this->result))
       <<" ended="<<ended.toString("hh:mm:ss");
    return result;
}

bool Result::isValid()
{
    return !qIsNaN(result);
}


}

