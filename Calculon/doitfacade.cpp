#include "doitfacade.h"
#include "doitlibrary.h"
DoItFacade::DoItFacade()
{

}

double DoItFacade::makeCalculation(Stc::Types::WorkType work, double operandA, double operandB, Stc::Types::Error &error)
{
    int errorCode=0;
    int workTypr=static_cast<int>(work);
    double result=DoIt(workTypr,operandA,operandB,errorCode);
    error=static_cast<Stc::Types::Error>(errorCode);
    return result;
}
