#include "queuerequestskeeper.h"
#include <QMutexLocker>

QueueRequestsKeeper &QueueRequestsKeeper::Instance()
{
    static QueueRequestsKeeper instance;
    return instance;
}

QueueRequestsKeeper::QueueRequestsKeeper():QObject(0),requestCounter(0){}

int QueueRequestsKeeper::addRequest(Request request)
{
    QMutexLocker lock(&mutex);
    request.uid=++requestCounter;
    QueueRequests<< request;
    emit sizeChanged(QueueRequests.size());
    return request.uid;
}

Request QueueRequestsKeeper::takeRequest()
{
    QMutexLocker lock(&mutex);
    if(QueueRequests.isEmpty())
        return Request();

    auto var=QueueRequests.takeFirst();
    emit sizeChanged(QueueRequests.size());
    return var;
}

int QueueRequestsKeeper::size()
{
    QMutexLocker lock(&mutex);
    return QueueRequests.size();
}

int QueueRequestsKeeper::totalTime()
{
     QMutexLocker lock(&mutex);
     int summ=0;
     foreach (auto var, QueueRequests)
     {
         summ+=var.durationSeconds;
     }
     return summ;
}



