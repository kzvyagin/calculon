#include "workertwo.h"
#include <QDebug>
#include "queueresultskepper.h"
#include "queuerequestskeeper.h"
WorkerTwo::WorkerTwo(QObject *parent) : QObject(parent)
{

}

void WorkerTwo::process()
{

}

void WorkerTwo::handleResultReady()
{
    if( QueueResultsKepper::Instance().size() == 0)
    {
        qDebug()<<"No results in results slot";
        return;
    }

    Stc::Result result=QueueResultsKepper::Instance().takeResult();
    if( result.isValid() )
        emit resultReady(result);
}

void WorkerTwo::handleRequest(Stc::Request request)
{
    qDebug()<<Q_FUNC_INFO<<request.toString();
    int uid=QueueRequestsKeeper::Instance().addRequest(request);
    request.uid=uid;
    emit requestAddedToQueue(request);
    emit handleRequest();
}
