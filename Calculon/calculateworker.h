#ifndef CALCULATEWORKER_H
#define CALCULATEWORKER_H

#include <QObject>
#include <QThread>
#include "stctypes.h"
#include <QJsonObject>


class CalculateWorker : public QObject
{
    Q_OBJECT

public:
    struct ConsoleColors
    {
        static QColor  ERROR ;
        static QColor  RESULT ;
        static QColor  REQUEST;

        static QColor  BACK_ERROR ;
        static QColor  BACK_RESULT ;
        static QColor  BACK_REQUEST;


    };
    explicit CalculateWorker(QObject *parent = 0);

    ///
    /// \brief work
    /// as example, not used
    void work();

    void initThreads();
    void putInformationToViewConsole(QString text, QColor color, QColor backColor);
signals:
    void resultComplite();

    void requestReleased( Stc::Request request );

    void resultToView(QString);

    void queueResultStatusChanged(QString qresstatus);
    void queueRequestStatusChanged(QString qreqstatus);

    void informationToConsoleReady(QJsonObject);
public slots:
    void errorHandler( QString error);

    ///
    /// \brief futureFinished
    /// as example, not used
    void futureFinished();

    void handleResult(Stc::Result result);
    void logRequest(Stc::Request request);

    void handleResultSizeChanged(int size);
    void handleRequestsizeChanged(int size);

    void handleViewConrtollerError(QString errorText);
};
#endif // CALCULATEWORKER_H
