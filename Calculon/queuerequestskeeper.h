#ifndef QUEUEREQUESTSKEEPER_H
#define QUEUEREQUESTSKEEPER_H

#include <QObject>
#include <QDateTime>
#include "doitfacade.h"
#include <QMutex>
#include "stctypes.h"
using namespace Stc;


class QueueRequestsKeeper : public QObject
{
    Q_OBJECT
public:
    static QueueRequestsKeeper& Instance();
private:
    QueueRequestsKeeper();
    QueueRequestsKeeper(const QueueRequestsKeeper& root);
    QueueRequestsKeeper& operator=(const QueueRequestsKeeper&);
signals:

    void sizeChanged(int);
public slots:
    int addRequest(Request request) ;
    Request takeRequest() ;
    int size();
    int totalTime();
private:
    QMutex mutex;
    int requestCounter;
    QList<Request> QueueRequests;
};

#endif // QUEUEREQUESTSKEEPER_H
