#ifndef VIEWCONTROLLER_H
#define VIEWCONTROLLER_H

#include <QObject>
#include <QVariant>
#include <qtypetraits.h>
#include "stctypes.h"
#include <QJsonObject>
class ViewController : public QObject
{
    Q_OBJECT
public:
    explicit ViewController(QObject *parent = 0);
    ~ViewController();

    Q_INVOKABLE void putExpressionIntoQueue(QString expression,int duration);

    Q_INVOKABLE QString putExpressionIntoQueueAsJson(QJsonObject obj);

    Q_INVOKABLE QJsonObject getGeometrySize();

    Q_INVOKABLE void saveGeometry(QJsonObject size);

signals:

    void requestReleased( Stc::Request request );

    void resultReady(QString resultString);
    void queueResultStatusChanged(QString qresstatus);
    void queueRequestStatusChanged(QString qreqstatus);

    void informationToConsoleReady(QJsonObject newElement);

    void rizeErrorToConsole(QString error);
public slots:

protected:
      void timerEvent(QTimerEvent *event);
      void flushGeometryToFile();
private:
    int settingsTimer;
    QString geometryString;
};

#endif // VIEWCONTROLLER_H
