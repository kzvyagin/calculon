#include "queuerequestsworker.h"
#include <QGuiApplication>
#include "doitfacade.h"
#include "queueresultskepper.h"
#include "queuerequestskeeper.h"
#include <QDebug>


QueueRequestsWorker::QueueRequestsWorker(QObject *parent) :
    QObject(parent)
  ,processTimerId(0)
{

}

QueueRequestsWorker::~QueueRequestsWorker()
{
}

void QueueRequestsWorker::process()
{
    handleIt();
}

void QueueRequestsWorker::processNewData()
{
    handleIt();
}

void QueueRequestsWorker::timerEvent(QTimerEvent *event)
{

    if( event->timerId() == processTimerId  )
    {
        killTimer(processTimerId);
        Result result=buffer.take(processTimerId);
        processTimerId=0;

        if( !result.isValid() )
            return;
        qDebug()<<Q_FUNC_INFO<<"end processing request at:"<<result.result<<result.errorCode<<QDateTime::currentDateTime().toString("mm:ss:zzz");
        result.ended=QDateTime::currentDateTime();
        QueueResultsKepper::Instance().addResult(result);
        emit resultReady();
    }

    handleIt( );
}

void QueueRequestsWorker::handleIt()
{
    qDebug()<<Q_FUNC_INFO<<"totoal requests queue size="<<QueueRequestsKeeper::Instance().size()<<" estimated time in seconds to process all requests="<<QueueRequestsKeeper::Instance().totalTime();
    if( !buffer.isEmpty()
        || QueueRequestsKeeper::Instance().size() == 0 )
    {
        qDebug()<<Q_FUNC_INFO<<"waiting for new requesrs...";
        return;
    }
    Request request=QueueRequestsKeeper::Instance().takeRequest();
    if( request.isValid() )
    {
        processRequest( request );
    }
}

void QueueRequestsWorker::processRequest(Request request)
{
    qDebug()<<Q_FUNC_INFO<<"start processing request at:"<<QDateTime::currentDateTime().toString("mm:ss:zzz");
    Result result;
    result.uid=request.uid;
    result.result=DoItFacade::makeCalculation(request.commandType,request.valueA,request.valueB,result.errorCode);
    processTimerId=startTimer(request.durationSeconds*1000);
    buffer.insert(processTimerId,result);
}
